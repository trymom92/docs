## How to sync Security repository with Canonical repository?

During the 'early-merge' phase of a security release, security merge requests targeting the
default branch are merged and deployed to GitLab.com at the same pace as regular security
merge requests. This causes Canonical and Security repositories to diverge.

Until the security release is published, the merge-train is automatically activated to keep
these repositories in sync. This automated task is handled by the [`security:merge_train` pipeline schedule],
which in turn executes the [`security:merge_train` task in release-tools].

Outside of this automation, there are two ways to sync the Security and Canonical changes.

### Using the pipeline schedule in Ops.

1. Go to https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
1. Click on `Play` on the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` pipeline schedule.

### Manually syncing the repositories

Use this option in case of a merge-train failure due to conflicts.


1. Clone the GitLab repository:

```ssh
git clone git@gitlab.com:gitlab-org/gitlab.git # This may take some time
```

2. Add the security remote:

```ssh
git add remote security git@gitlab.com:gitlab-org/security/gitlab.git
``` 

3. Fetch the content from both remotes:

```ssh
git fetch origin
git fetch security
```

4. Create a branch from `security/master`

```ssh
git checkout master --track security/master
git checkout  -b sync-security-with-canonical
```

5. Merge `origin/master` into your branch, at this point conflicts will need to be resolved

```ssh
git merge origin/master`
```

6. Push the changes into `security` (**be careful of not pushing the changes to Canonical**)


```ssh
git push security sync-security-with-canonical
```

7. Create a merge request and assign it to your release manager counterpart for approval, then set MWPS.

After these steps, upcoming merge train jobs shouldn't fail anymore.

[`security:merge_train` task in release-tools]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/security.rake#L31
[`security:merge_train` pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit
