**Current:** is the last successful deployed package. It may not represent the entire fleet, as it may exist an ongoing deployment

**Previous:** is the last successful deployed package prior to the current package.

**New:** is a package version that is being deployed at the time of speaking.
